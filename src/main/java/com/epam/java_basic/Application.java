package com.epam.java_basic;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
 import com.epam.java_basic.calculator.Calculator;
 import java.io.*;
 import java.util.*;


public class Application {

    public static void main(String[] args) {
        throw new UnsupportedOperationException("You need to implement this method");
Scanner sc=new Scanner(System.in);
Calculator cal=new Calculator();
do{
System.out.println("Enter the first number:");
double first=sc.nextDouble();
System.out.println("Enter the second number:");
double second=sc.nextDouble();
System.out.println("Enter operator (+, -, *, /):");
char operator=sc.next().charAt(0);
switch(operator){
case '+':
	double result=cal.add(first,second);
	break;
case '-':
	double result=cal.sub(first,second);
	break;
case '*':
	double result=cal.multiply(first,second);
	break;
case '/':
	double result=cal.div(first,second);
	break;
}
System.out.print("Result: ");
System.out.format("%.1f",result);
System.out.println("Do you want to continue? (Y/N)");
char answer=sc.nextchatAt(0);
}while(answer!='N');
System.out.println("Bye!");

    }

}
